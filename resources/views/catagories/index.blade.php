@extends('layouts.admin-panel.app')

@section('content')
<div class="d-flex justify-content-end mb-3">
    <a href="{{route('catagories.create')}}" class="btn btn-outline-primary">Add Catagory</a>
</div>
<div class="card">
    <div class="card-header">
        <h2>catagories</h2>
    </div>
    <div class="card-body">
        <ul class="list-group list-group-flush">
            @foreach ($catagories as $catagory)
                <li class="list-group-item">{{ $catagory->name }}</li>
            @endforeach
        </ul>
    </div>
</div>
<div class="mt-5">
    {{ $catagories->links('vendor.pagination.bootstrap-4') }}
</div>
@endsection
